// console.log("Hello");

// List of StudentIDs to all graduating studnet of the class.
console.log("Variable vs Array");

let studentNumberA = "2022-1923";
let studentNumberB = "2022-1924";
let studentNumberC = "2022-1925";
let studentNumberD = "2022-2026";
let studentNumberE = "2022-2027";

console.log(studentNumberA);
console.log(studentNumberB);
console.log(studentNumberC);
console.log(studentNumberD);
console.log(studentNumberE);
						// 0      //1            //2		//3    		//4 - index
let studenNumbers = ["2022-1923","2022-1924","2022-1925","2022-2026","2022-2027"];
// Inside the square bracket aree what we called elements
console.log(studenNumbers);


// [SECTION] Arrays
/*
        - Arrays are used to store multiple related values in a single variable.
        - They are declared using square brackets ([]) also known as "Array Literals"
        - Arrays it also provides access to a number of functions/methods that help in manipulation array.
            - Methods are used to manipulate information stored within the same object.
        - Array are also objects which is another data type.
        - The main difference of arrays with object is that it contains information in a form of "list" unlike objects which uses "properties" (key-value pair).
        - Syntax:
            let/const arrayName = [elementA, elementB, elementC, ... , ElementNth];
    */
console.log("Examples of Array: ");

	// Common examples of arrays
	let grades = [98.5, 94.3,89.2,90] // arrays could store numeric values.
	let computerBrand = ["Acer", "Asus", "Lenovo","Neo","Redfox","Toshiba","Fujitsu"]; // arrays could store string vallues
	console.log(grades);
	console.log(computerBrand);
	// Possible use of an array but it is not recommended
	// IT is not recommended for it is hard to define what is the purpose of the array or what are data inside the array and where the programmer or co-programmer could use it.
	let mixedArr = ["Jhon", "Doe",12,false, null, undefined, {} ];
	console.log(mixedArr);

// Alternative way of creating an array
let myTasks = [
"drink html",
"eat javascript",
"inhale css",
"bake mongoDb"
];
console.log(myTasks);

console.log("---------------------------");
let city1= "Tokyo";
let city2="Manila";
let city3= "Jakarta";


let cities = [city1,city2,city3];
console.log(cities);

console.log("---------------------------");

// [SECTION] .length property
console.log("Using .length property for array size");
console.log("length / size of myTasks array: " + myTasks.length+ "."); // Output should be 4
console.log("length / size of cities array: " + cities.length+ "."); ; // Output should be 3
console.log("------------------");
console.log("Using .length property for string size");

let fullName = "Romulo Baal Jr";
console.log("Length/ size of fullName string: " + fullName.length);
console.log("------------------");
console.log("Removing the last element from an array");
myTasks.length = myTasks.length -1;
// removes the last value
console.log("length / size of myTasks array: " + myTasks.length+ ".");
console.log(myTasks);

// To delete a specific item in an array we can emply array methods. We have shown the logic or algorithm of "pop";

// let cities = [city1,city2,city3];
cities.length--; //2 values left
console.log(cities);

//We can't do the same on string 
fullName.length =  fullName.length -1
console.log(fullName.length);
console.log(fullName);
console.log("------------------");
// We can also add the length of an array.
console.log("Add an element to an array:");
					//0 	//1    //2     //3
let theBeatles = ["John", "Paul","Ringo","George"];
console.log(theBeatles);

theBeatles[4] = "Cardo";
console.log(theBeatles);

theBeatles[theBeatles.length] = "Juan"
console.log(theBeatles);

console.log("--------------------------------");

// [SECTION] Reading from Arrays
    /*
        - Accessing array elements is one of the common task that we do with an array.
        - This can be done through the use of array indexes.
        - Each element in an array is associated with it's own index number.
        - The first element in an array is associated with the number 0, and increasing this number by 1 for every element.
        - Array indexes it is actually refer to a memory address/location

        Array Address: 0x7ffe942bad0
        Array[0] = 0x7ffe942bad0
        Array[1] = 0x7ffe942bad4
        Array[2] = 0x7ffe942bad8

        - Syntax
            arrayName[index];
    */
    // let grades = [98.5, 94.3,89.2,90] 
    console.log(grades[0]);

    console.log(computerBrand[3]);


  // Accessing an array element that does not exist 
  console.log(grades[20]);

  let lakersLegends=["Kobe","Shaq","Lebron","Magic","Kareem"];
  // accessing and display shaq and magic
  console.log(lakersLegends[1]+ " & " + lakersLegends[3]);
  // console.log();
console.log("--------------------------------");
console.log("Reaassigning an element from an array.");
	// You can also reassign array values using indeces 

	console.log("Array before Reaassigning: ");
	console.log(lakersLegends);


lakersLegends[2] = "Gasol";

	console.log("Array after Reaassigning: ");
console.log(lakersLegends);
console.log("--------------------------------");

console.log("Acess the last element of an Array");

						//0 	//1      //2 	  //3    //4
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegends.length-1; 
	console.log(bullsLegends[lastElementIndex]);

	console.log("--------------------------");
console.log("Adding a new items into an array using indeces");
	// Adding Items into an array
	// We can add items in array using indeces


	const newArray = [];

	console.log(newArray[0]); // undefined with no value

	newArray[0] = "Cloud Strife";
	console.log(newArray);

	console.log(newArray[1]); //undefined

	newArray[1] = "Tife Lockhart";
	console.log(newArray);

console.log("--------------------------");
console.log("Add element in the end of an array using .length property");

// ["Cloud Strife","Tife Lockhart"]
newArray[newArray.length] = "Barret Wallace";
console.log(newArray);
console.log("--------------------------");

console.log("Displaying the content of an array 1 by 1");
// ["Cloud Strife","Tife Lockhart","Barret Wallace"]
// Example of accessing array element 1 by 1 with console.log
// console.log(newArray[0]);
// console.log(newArray[1]);
// console.log(newArray[2]);

// Looping over an array 
	//declaration (let i=0) 
	// condition i< newArray.length;
	// change of value i++
for (let index=0; index< newArray.length; index++ ) {
	// To be able to show each array items in the console.log
	console.log(newArray[index]);
}
console.log("--------------------------");
console.log("Filteriing an array using loop and conditional statements: ");
let numArray1 = [5,12,30,46,40,52];
// This code to check which number is divisible by 5
for (let index=0; index<numArray1.length; index++){
	if(numArray1[index] % 5 == 0){
		console.log( numArray1[index] + " Divisible by 5");
	}else{
		console.log(numArray1[index] + " Not divisible by 5");
	}
}
// [SECTION] Multidimensional Arrays
/*
    -Multidimensional arrays are useful for storing complex data structures.
    - A practical application of this is to help visualize/create real world objects.
    - This is frequently used to store data for mathematic computations, image processing, and record management.
    - Array within an Array
*/
// Create Chesboard
let chessBoard = 
	[
    ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
    ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
    ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
    ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
    ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
    ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
    ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
    ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];

console.table(chessBoard);
					//row // column
console.log(chessBoard[3][4]); //e4

console.log("Pawns moves to :" + chessBoard[2][5]);